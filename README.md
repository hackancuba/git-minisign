# git-minisign

Scripts to sign and verify Git tags, commits, or objects in general, using Minisign.

## How does it work

* `git-minisign-sign.sh`

    Signs the tag, commit, or object under the given hash and then saves the signature to Git notes. If the commit object with the given hash does not exist, it first tries to unpack `.pack` files in `.git/objects/pack`.

    Once you do `git push`, you also have to do `git push remote_name refs/notes/commits` to push the notes to the repo.

* `git-minisign-verify.sh`

    Fetches commit notes, then extracts the signature from the note of the commit under the given hash, checks if the commit is unpacked, unpacks the commit if needed, and then verifies the signature.

## Signing

Tags, commits, or objects are signed using [Minisign](https://jedisct1.github.io/minisign/), and signatures are stored in git notes.

Pubkeys:

* HacKan: `RWRcT0IUOJ7kj6AFLyI3pHmT6dhr+WN8C2FR6HguMmEK0MnsSImqSmjg`
* SkyFox42: `RWSxb5gftMn69bFHSOfM7flSRDB/eSjBpz97oXOZD0amGuybqyJ5JYL+`

## License

**git-minisign** is made by [HacKan](https://hackan.net), forked of [git-minisign](https://codeberg.org/SkyFox42/git-minisign) by [SkyFox42](https://codeberg.org/SkyFox42), under MIT. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).  
Derived works may link back to the canonical repository: `https://gitlab.com/hackancuba/git-minisign`.

    MIT License Copyright (c) 2022 HacKan (https://hackan.net)
    MIT License Copyright (c) 2021 SkyFox42
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is furnished
    to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice (including the next
    paragraph) shall be included in all copies or substantial portions of the
    Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
    OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
    OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
