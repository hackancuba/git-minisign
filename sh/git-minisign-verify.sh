#!/bin/bash
set -euo pipefail

help() {
    echo "Usage: git-minisign-verify [OPTIONS]"
    echo "Verify a Minisign signature of a Git tag, commit, or object"
    echo
    echo "            -h            show help"
    echo "            -v            output version"
    echo "            -p [FILE]     Specify the public key file for verification. If not"
    echo "                          specified, and no pubkey, defaults to ./minisign.pub"
    echo "            -P [PUBKEY]   Specify the public key for verification"
    echo "            -H [HASH]     The hash of commit, or object, to verify. If not set, will ask"
    echo "                          interactively. Defaults to HEAD"
    echo "            -T [TAG]      The name of the tag to verify"
    echo
    echo "Note that if both the pubkey and the pubkey file are specified, only the pubkey is used."
    echo "If none is specified, minisign expects the file minisign.pub to exist, otherwise it will"
    echo "fail."
}

version() {
    echo "git-minisign-verify v0.3 by SkyFox42, licensed under MIT"
}

check_minisign() {
    if [ ! -x "$(command -v minisign)" ]; then
        echo "Could not find Minisign. Aborting"
        exit 1
    fi
}

check_git_repo() {
    if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" != "true" ]; then
        echo "Current folder is not a Git repo. Aborting"
        exit 1
    fi
}

validate_hash() {
    if [[ -n "$TAG" ]]; then
        if ! git show-ref --tags "$TAG" --quiet; then
            echo "Tag does not exist: $TAG. Aborting"
            exit 1
        fi

        HASH="$(git tag --list --format '%(objectname)' "$TAG")"
        echo "Using tag $TAG ($HASH)"
    elif [[ -z "$HASH" ]]; then
        read -r -p "Enter the commit hash [HEAD]: " HASH
    fi

    if [[ -z "$HASH" || "${HASH,,}" == "head" ]]; then
        HASH="$(git rev-parse HEAD)"
        echo "Using HEAD instead (${HASH})"
    fi

    # Parse hash in case a short one is used
    HASH="$(git rev-parse "$HASH")"
}

ensure_commit_exists() {
    echo "Checking if the commit is unpacked..."
    if [ ! -e ".git/objects/${HASH:0:2}/${HASH:2}" ]; then
        echo "Unpacking the commits..."
        PACKSDIR="$(mktemp --directory --tmpdir minisign-pack.XXXXXXXXXX)"
        mv .git/objects/pack/* "$PACKSDIR"
        for object in "$PACKSDIR"/*.pack; do
            echo "Unpacking $object..."
            git unpack-objects < "$object"
        done
        rm -rf "${PACKSDIR}"  # Don't trap this deletion in case something fails above
    fi

    if [ ! -e ".git/objects/${HASH:0:2}/${HASH:2}" ]; then
        echo "The commit does not exist. Aborting"
        exit 1
    fi
}

fetch_notes() {
    echo "Fetching git notes..."
    git fetch origin refs/notes/commits:refs/notes/commits
}

verify_sig() {
    echo "Getting the signature for commit ${HASH}..."
    git notes show "$HASH" > "$MINISIGFILE"

    echo "Verifying the signature..."
    echo
    local -a minisign_args=( "-Vm" ".git/objects/${HASH:0:2}/${HASH:2}" "-x" "$MINISIGFILE" )
    if [[ -n "$PUBKEY" ]]; then
        minisign_args+=( "-P" "$PUBKEY" )
    elif [[ -n "$PUBKEYFILE" ]]; then
        minisign_args+=( "-p" "$PUBKEYFILE" )
    fi
    minisign "${minisign_args[@]}"
}

cleanup() {
    rm -f "${MINISIGFILE}" >/dev/null 2>&1
}

MINISIGFILE="$(mktemp --tmpdir minisign-sig.XXXXXXXXXX)"
PUBKEYFILE=""
PUBKEY=""
HASH=""
TAG=""

trap cleanup EXIT

check_minisign
check_git_repo

# Parse args
while getopts ":P:H:T:p:hv" OPTION 2> /dev/null
do
    case $OPTION in
        h) help
            exit;;
        v) version
            exit;;
        P) PUBKEY="$OPTARG";;
        p) PUBKEYFILE="${OPTARG:-minisign.pub}";;
        H) HASH="$OPTARG";;
        T) TAG="$OPTARG";;
        ?) echo "Option $OPTARG does not exist."
            exit 1;;
    esac
done
shift  $((OPTIND - 1))

validate_hash
ensure_commit_exists

fetch_notes
verify_sig
