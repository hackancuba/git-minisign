#!/bin/bash
set -euo pipefail

help() {
    echo "Usage: git-minisign-sign [OPTIONS]"
    echo "Sign a Git tag, commit, or object, with Minisign"
    echo
    echo "            -h            show help"
    echo "            -v            output version"
    echo "            -S [SECKEY]   Secret key file (default: ~/.minisign/minisign.key)"
    echo "            -H [HASH]     The hash of the commit, or object, to sign. If not set, will"
    echo "                          ask interactively. Defaults to HEAD"
    echo "            -T [TAG]      The name of the tag to sign"
    echo "            -c [COMMENT]  Add a one-line untrusted comment"
    echo "            -t [COMMENT]  Add a one-line trusted comment"
    echo "            -f            Force overwriting an existing signature"
}

version() {
    echo "git-minisign-sign v0.3 by SkyFox42, licensed under MIT"
}

check_minisign() {
    if [ ! -x "$(command -v minisign)" ]; then
        echo "Could not find Minisign. Aborting"
        exit 1
    fi
}

check_git_repo() {
    if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" != "true" ]; then
        echo "Current folder is not a Git repo. Aborting"
        exit 1
    fi
}

validate_hash() {
    if [[ -n "$TAG" ]]; then
        if ! git show-ref --tags "$TAG" --quiet; then
            echo "Tag does not exist: $TAG. Aborting"
            exit 1
        fi

        HASH="$(git tag --list --format '%(objectname)' "$TAG")"
        echo "Using tag $TAG ($HASH)"
    elif [[ -z "$HASH" ]]; then
        read -r -p "Enter the commit hash [HEAD]: " HASH
    fi

    if [[ -z "$HASH" || "${HASH,,}" == "head" ]]; then
        HASH="$(git rev-parse HEAD)"
        echo "Using HEAD instead (${HASH})"
    fi

    # Parse hash in case a short one is used
    HASH="$(git rev-parse "$HASH")"
}

ensure_commit_exists() {
    echo "Checking if the commit is unpacked..."
    if [ ! -e ".git/objects/${HASH:0:2}/${HASH:2}" ]; then
        echo "Unpacking the commits..."
        PACKSDIR="$(mktemp --directory --tmpdir minisign-pack.XXXXXXXXXX)"
        mv .git/objects/pack/* "$PACKSDIR"
        for object in "$PACKSDIR"/*.pack; do
            echo "Unpacking $object..."
            git unpack-objects < "$object"
        done
        rm -rf "${PACKSDIR}"  # Don't trap this deletion in case something fails above
    fi

    if [ ! -e ".git/objects/${HASH:0:2}/${HASH:2}" ]; then
        echo "The commit does not exist. Aborting"
        exit 1
    fi
}

create_sig() {
    echo "Creating the signature..."

    local -a minisign_args=( "-Sm" ".git/objects/${HASH:0:2}/${HASH:2}" "-x" "$MINISIGFILE" "-s" "$SECKEYFILE" )
    if [[ -n "$TRUSTEDCOMMENT" ]]; then
      minisign_args+=( "-t" "$TRUSTEDCOMMENT" )
    fi
    if [[ -n "$UNTRUSTEDCOMMENT" ]]; then
      minisign_args+=( "-c" "$UNTRUSTEDCOMMENT" )
    fi

    minisign "${minisign_args[@]}"
}

add_sig() {
    local -a args=( "--file" "$MINISIGFILE" )
    if [[ "$FORCE" = true ]]; then
        args+=( "--force" )
    fi

    echo "Adding the signature as a note..."
    git notes add "${args[@]}" "$HASH"
}

cleanup() {
    rm -f "${MINISIGFILE}" >/dev/null 2>&1
}

SECKEYFILE="$HOME/.minisign/minisign.key"
MINISIGFILE="$(mktemp --tmpdir minisign-sig.XXXXXXXXXX)"
TRUSTEDCOMMENT=""
UNTRUSTEDCOMMENT=""
FORCE=false
HASH=""
TAG=""

trap cleanup EXIT

check_minisign
check_git_repo

# Parse args
while getopts ":S:H:c:t:T:hvf" OPTION 2> /dev/null
do
    case $OPTION in
        h) help
            exit;;
        v) version
            exit;;
        S) SECKEYFILE="$OPTARG";;
        H) HASH="$OPTARG";;
        c) UNTRUSTEDCOMMENT="$OPTARG";;
        t) TRUSTEDCOMMENT="$OPTARG";;
        T) TAG="$OPTARG";;
        f) FORCE=true;;
        ?) echo "Option $OPTARG does not exist."
            exit 1;;
    esac
done
shift  $((OPTIND - 1))

validate_hash
ensure_commit_exists

create_sig
add_sig

echo
echo "Done. Don't forget to push the notes:"
echo "git push origin refs/notes/commits"
echo
echo "You can see the signature with:"
echo "git notes show $HASH"
